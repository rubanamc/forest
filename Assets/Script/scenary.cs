﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class scenary : MonoBehaviour
{
    public GameObject fore1;
    public GameObject fore2;
    public GameObject fore3;

    public GameObject ground1;
    public GameObject ground2;
    public GameObject ground3;
    public GameObject middle1;
    public GameObject middle2;
    public GameObject middle3;
    public GameObject bg1;
    public GameObject bg2;
    public GameObject bg3;
    public GameObject sky1;
    public GameObject sky2;
    public GameObject sky3;
    public GameObject obstacles;
    private IEnumerator coroutine;


    void Start()
    {
        coroutine = WaitAndPrint(2.0f);
    StartCoroutine(coroutine);

    print("Before WaitAndPrint Finishes " + Time.time);
    }

// every 2 seconds perform the print()
private IEnumerator WaitAndPrint(float waitTime)
   {
    while (true)
    {
        yield return new WaitForSeconds(waitTime);
        print("WaitAndPrint " + Time.time);
    }
}

    // Update is called once per frame
    void Update()
    {
        
        void fore()
        {
            move(-20.61f,33.50f,4f,fore1,fore2,fore3);
        }

        fore();

       
       void middle()
        {
           move(-21.09f, 36.40f, 3f, middle1, middle2,middle3);
        }
        middle();
       void ground()
        {
            move(-21.17f,36.20f,6f,ground1,ground2,ground3);
        }
        ground();
        void back()
       {
           move(-20.45f, 33.23f, 1.5f, bg1, bg2, bg3);
       }
        back();
        void sky()
        {
           move(-21.13f,35.16f,1f,sky1,sky2,sky3);
        }
        sky();
        //fore();
        //middle();
        //ground();
        //back();sky();




        void move(float leftpos, float rightpos, float speed, GameObject objname1, GameObject objname2, GameObject objname3)
        {

            
            if (objname1.transform.position.x < leftpos)
            {
                objname1.transform.position = new Vector3(rightpos, objname1.transform.position.y, objname1.transform.position.z);
            }

            if (objname2.transform.position.x <leftpos)
            {
                objname2.transform.position = new Vector3(rightpos, objname2.transform.position.y, objname2.transform.position.z);
            }
            if (objname3.transform.position.x < leftpos)
            {
                objname3.transform.position = new Vector3(rightpos, objname3.transform.position.y, objname3.transform.position.z);
            }
            objname1.transform.Translate(Vector3.left * Time.deltaTime * speed);
            objname2.transform.Translate(Vector3.left * Time.deltaTime * speed);
            objname3.transform.Translate(Vector3.left * Time.deltaTime * speed);
        }

        

    }

}
